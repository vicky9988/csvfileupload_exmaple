package com.upwork.main;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fileupload.main.controller.EmployeeCSVController;
import com.fileupload.main.service.IProcessCSVRecords;

@SpringBootTest
class CSVFileUploadApplicationTests {

	@Autowired
	IProcessCSVRecords csvPro;
	
	 @Autowired
	    private MockMvc mockMvc;

	 @Test
	    void uploadingCSVStatus200OK() throws Exception {
	        MockMultipartFile mockMultipartFile = new MockMultipartFile(
	                "multipartFile",
	                "Employee_1.csv",
	                "text/csv",
	                new ClassPathResource("Employee_1.csv").getInputStream());

	        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/uploadcsv")
	                .file(mockMultipartFile))
	                .andExpect(status().isOk());
	} 
	 
	 @Test
	  public void test_handleFileUpload_NoFileProvided() throws Exception{
	    MockMultipartHttpServletRequestBuilder multipartRequest =
	        MockMvcRequestBuilders.multipart("/api/uploadcsv");
	 
	    mockMvc.perform(multipartRequest)
	        .andExpect(status().isBadRequest());
	  }

}


