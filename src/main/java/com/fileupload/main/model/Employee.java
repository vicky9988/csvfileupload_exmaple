package com.fileupload.main.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.*;


@Getter
@Setter
@Accessors(chain=true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="employee")
public class Employee {

	@CsvBindByName
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private int id;
	
	@CsvBindByName
	private String name;
	
	@CsvBindByName
	private int age;
	
	@CsvBindByName
	private String country;
	
	
	
}

