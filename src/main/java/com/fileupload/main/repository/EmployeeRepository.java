package com.fileupload.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fileupload.main.model.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}
