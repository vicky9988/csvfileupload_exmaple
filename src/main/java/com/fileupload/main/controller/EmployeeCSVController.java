package com.fileupload.main.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fileupload.main.model.Employee;
import com.fileupload.main.service.IEmployeeService;
import com.fileupload.main.service.IProcessCSVRecords;
import com.fileupload.main.service.ProcessCSVRecords;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;


@RestController
@RequestMapping("/api")
public class EmployeeCSVController {
	
	@Autowired
	IEmployeeService empService;
	
	@Autowired
	IProcessCSVRecords csvRec;
	
	@GetMapping("/hello")
	public String getResult() {
		return "Hello.......";
	}
	
	
	 @PostMapping(value ="/uploadcsv", consumes= {MediaType.MULTIPART_FORM_DATA_VALUE},
		     produces = {MediaType.APPLICATION_JSON_VALUE} )	 
		public String uploadCSVFile(@RequestBody MultipartFile file) {
			String result = "";
			List<Employee> employeeList = new ArrayList<>();
			if (file.isEmpty()) {

			} else {

				employeeList = csvRec.getCSVRecords(file);
				for (Employee emp : employeeList) {
					empService.save(emp);
				}
				result = "Success insertion:- " + ProcessCSVRecords.getSuccessCount() + "/n Faliure Count:- "
						+ ProcessCSVRecords.getFailureCount();
			}
			return result;

		}
}
