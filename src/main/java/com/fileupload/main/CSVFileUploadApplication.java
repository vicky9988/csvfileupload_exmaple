package com.fileupload.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.upwork.main")
public class CSVFileUploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(CSVFileUploadApplication.class, args);
	}

}
