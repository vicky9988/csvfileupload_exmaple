package com.fileupload.main.service;

import com.fileupload.main.model.Employee;


public interface IEmployeeService {

	Employee save(Employee e);
}
