package com.fileupload.main.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fileupload.main.model.Employee;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

@Service
public class ProcessCSVRecords implements IProcessCSVRecords {

	public static int failRecordCount=0;
	public static int successRecordCount=0;
	
	public List<Employee> getCSVRecords(MultipartFile file) {
		List<Employee> empList= new ArrayList<>();
		try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

			// create csv bean reader
			CsvToBean<Employee> csvToBean = new CsvToBeanBuilder<Employee>(reader).withType(Employee.class)
					.withIgnoreLeadingWhiteSpace(true).build();
			 empList = getListOfEmployee(csvToBean);
			
			return empList;

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return empList;
	}

	private List<Employee> getListOfEmployee(CsvToBean<Employee> csvToBean) {
		List<Employee> empList= new ArrayList<>();
		try {
		for(Employee emp: csvToBean.parse()) {
			successRecordCount=successRecordCount+1;
			empList.add(emp);
		}
		
		}catch(Exception e) {
			failRecordCount = failRecordCount+1;
		}
		return empList;
	}
	
	public static int getSuccessCount() {
		return successRecordCount;
	}
	
	public static int getFailureCount() {
		return failRecordCount;
	}


	
}
