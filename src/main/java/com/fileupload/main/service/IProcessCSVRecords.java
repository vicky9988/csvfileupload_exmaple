package com.fileupload.main.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fileupload.main.model.Employee;

public interface IProcessCSVRecords {

	

	List<Employee> getCSVRecords(MultipartFile file );
}
