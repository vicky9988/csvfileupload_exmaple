package com.fileupload.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fileupload.main.model.Employee;
import com.fileupload.main.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements IEmployeeService{

	@Autowired
	EmployeeRepository empRepo;
	
	@Override
	public Employee save(Employee e) {
		// TODO Auto-generated method stub
		 return empRepo.save(e);
	}

}
